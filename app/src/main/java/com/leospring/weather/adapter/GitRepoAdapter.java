package com.leospring.weather.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.leospring.weather.activity.R;
import com.leospring.weather.bean.GitContent;
import com.leospring.weather.bean.GitRepository;

import java.util.List;

/**
 * Created by Administrator on 2018/11/12.
 */

public class GitRepoAdapter extends ArrayAdapter {
    private final int resourceId;
    public GitRepoAdapter(@NonNull Context context, int resource, @NonNull List objects) {
        super(context, resource, objects);
        resourceId = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        GitContent gitRepos = (GitContent) getItem(position); // 获取当前项的Fruit实例
        View view = LayoutInflater.from(getContext()).inflate(resourceId, null);//实例化一个对象
        ImageView fruitImage = (ImageView) view.findViewById(R.id.git_file_image);//获取该布局内的图片视图
        TextView fruitName = (TextView) view.findViewById(R.id.git_file_name);//获取该布局内的文本视图
        String type = gitRepos.getType();
        if(type.equals("file")) {
            fruitImage.setImageResource(R.drawable.file);
        }else {
            fruitImage.setImageResource(R.drawable.folder3);
        }
//        fruitImage.setImageResource(gitRepos.getImageId());//为图片视图设置图片资源
        fruitName.setText(gitRepos.getName());//为文本视图设置文本内容
        return view;
    }
}
