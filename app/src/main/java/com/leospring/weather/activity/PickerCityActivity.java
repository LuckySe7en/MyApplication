package com.leospring.weather.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.bigkoo.pickerview.OptionsPickerView;
import com.itheima.retrofitutils.ItheimaHttp;
import com.itheima.retrofitutils.Request;
import com.itheima.retrofitutils.listener.HttpResponseListener;
import com.leospring.weather.bean.City;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Headers;
import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * Created by Administrator on 2018/10/31.
 */

public class PickerCityActivity extends AppCompatActivity {

    private TextView mTextViewAddress;

    OptionsPickerView pvOptions;

    private static final int INIT_VIEW = 1;

    private static final int OPEN_DIALOG = 2;

    private static List<City.ResultBean> provinceBeans = null;

    //  省份
    ArrayList<String> provinceBeanList = new ArrayList<>();
    //  城市
    ArrayList<List<String>> cityList = new ArrayList<>();
    //  区/县
    ArrayList<List<List<String>>> districtList = new ArrayList<>();

    private Handler mHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case INIT_VIEW:
                    initView((City.ResultBean) msg.obj);
                    break;
                case OPEN_DIALOG:
                    openDialog();
                    break;
                default:
                    break;
            }
        }

    };

    private void openDialog() {

        parseData();
        //  创建选项选择器
        pvOptions = new OptionsPickerView(this);


        //  设置三级联动效果
        pvOptions.setPicker(provinceBeanList, cityList, districtList, true);


        //  设置选择的三级单位
        //pvOptions.setLabels("省", "市", "区");
        //pvOptions.setTitle("选择城市");

        //  设置是否循环滚动
        pvOptions.setCyclic(false, false, false);


        // 设置默认选中的三级项目
        pvOptions.setSelectOptions(0, 0, 0);
        //  监听确定选择按钮
        pvOptions.setOnoptionsSelectListener(new OptionsPickerView.OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int option2, int options3) {
                //返回的分别是三个级别的选中位置
                String city = provinceBeanList.get(options1);
                String address;
                //  如果是直辖市或者特别行政区只设置市和区/县
                if ("北京".equals(city) || "上海".equals(city) || "天津".equals(city) || "重庆".equals(city) || "澳门".equals(city) || "香港".equals(city)) {
                    address = provinceBeanList.get(options1)
                            + " " + districtList.get(options1).get(option2).get(options3);
                } else {
                    address = provinceBeanList.get(options1)
                            + " " + cityList.get(options1).get(option2)
                            + " " + districtList.get(options1).get(option2).get(options3);
                }
                mTextViewAddress.setText(address);
            }
        });
        pvOptions.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ItheimaHttp.init(this, "http://apicloud.mob.com/v1/weather/");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.piker_city);

        mTextViewAddress = (TextView) findViewById(R.id.tv_address);

        //  点击弹出选项选择器
        mTextViewAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Message message = new Message();
                message.what = OPEN_DIALOG;
                message.obj = 1;

                mHandler.sendMessage(message);
            }
        });

        queryCity();





    }

    private void initView(City.ResultBean bean){

        parseData();



    }
    /**
     * 填充数据
     */
    private void parseData() {
        cityList = new ArrayList<>();
        districtList = new ArrayList<>();
        for (City.ResultBean provinceBean : provinceBeans) {
            provinceBeanList.add(provinceBean.getProvince());
            List<String> city = new ArrayList<>();
            List<List<String>> areaList = new ArrayList<>();
            for (City.ResultBean.CityBean cityBean : provinceBean.getCity()) {
                city.add(cityBean.getCity());

                List<String> area = new ArrayList<>();
                for (City.ResultBean.CityBean.DistrictBean districtBean : cityBean.getDistrict()) {
                    area.add(districtBean.getDistrict());
                }
                areaList.add(area);
            }
            cityList.add(city);
            districtList.add(areaList);
        }
    }

    private void queryCity() {
        Request request = ItheimaHttp.newGetRequest("citys?key=286e5b4c7b9a3");//apiUrl格式："xxx/xxxxx"
        Call call = ItheimaHttp.send(request, new HttpResponseListener<City>() {
            @Override
            public void onResponse(City bean, Headers headers) {
                provinceBeans = bean.getResult();
                Log.d("Weather", JSONObject.wrap(bean).toString());
//                Message message = new Message();
//                message.what = INIT_VIEW;
//                message.obj = provinceBeans;
//
//                mHandler.sendMessage(message);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable e) {
                Log.d("Weather",e.getLocalizedMessage());
            }
        });
    }
}