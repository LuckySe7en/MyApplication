package com.leospring.weather.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.itheima.retrofitutils.ItheimaHttp;
import com.itheima.retrofitutils.Request;
import com.itheima.retrofitutils.listener.HttpResponseListener;
import com.leospring.weather.adapter.GitRepoAdapter;
import com.leospring.weather.bean.GitContent;
import com.leospring.weather.cache.GitCache;

import java.util.List;

import okhttp3.Headers;
import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * Created by Administrator on 2018/11/13.
 */

public class GitContentActivity extends AppCompatActivity {

    private static final int SUCCESS = 1;
    private static final int EDIT_FILE = 2;
    private ListView listView;
    private TextView textView;

    private List<GitContent> data;

    private Handler mHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case SUCCESS:
                    showAdapter(msg.obj);
                    break;
                case EDIT_FILE:
                    openEdit(msg.obj);
                default:
                    break;
            }
        }

    };

    private void openEdit(Object obj) {
        Intent intent = new Intent(this,GitEditActivity.class);
        intent.putExtra("path",obj.toString());
        startActivity(intent);
    }

    private void showAdapter(Object obj) {
        GitRepoAdapter adapter = new GitRepoAdapter(this,R.layout.git_content_item,data);
        listView.setAdapter(adapter);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        ItheimaHttp.init(this, "https://api.github.com/");
        String reposName = getIntent().getStringExtra("reposName");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.git_content);
        listView = findViewById(R.id.content_list_view);
        textView = findViewById(R.id.git_path);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ImageView image = view.findViewById(R.id.git_file_image);
                TextView fileName = view.findViewById(R.id.git_file_name);
                GitContent gitContent = data.get(position);

                if(gitContent.getType().equals("dir")) {
                    init(reposName,gitContent.getPath());
                }else {
                    Message message = new Message();
                    message.what = EDIT_FILE;
                    message.obj = "repos/" + GitCache.getGitUser().getLogin() +"/" + reposName + "/contents/" + gitContent.getPath();
                    mHandler.sendMessage(message);

                }
            }
        });

        init(reposName,"");

    }

    private void init(String reposName,String path){
        Request request = ItheimaHttp.newGetRequest("repos/" + GitCache.getGitUser().getLogin() +"/" + reposName + "/contents/" + path);
        request.putHeader("Authorization",GitCache.getGitUser().getAuthorization());
        textView.setText(reposName + "/" + path);
        ItheimaHttp.send(request, new HttpResponseListener<List<GitContent>>() {
            @Override
            public void onResponse(List<GitContent> beans, Headers headers) {
                data = beans;
                Message message = new Message();
                message.what = SUCCESS;
                mHandler.sendMessage(message);
//                List<String> data = new ArrayList<>();
//                for (GitContent bean : beans) {
//                    data.add(bean.getName() + "\r" + bean.getDescription());
//                }
//                ArrayAdapter<String> adapter = new ArrayAdapter<String>(
//                        GitUserInfoActivity.this, android.R.layout.simple_list_item_1, data);
//                listView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable e) {
            }
        });
    }
}
