package com.leospring.weather.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.itheima.retrofitutils.ItheimaHttp;
import com.itheima.retrofitutils.Request;
import com.itheima.retrofitutils.listener.HttpResponseListener;
import com.leospring.weather.bean.GitUser;
import com.leospring.weather.cache.GitCache;
import com.leospring.weather.utils.Base64Utils;

import okhttp3.Headers;

/**
 * Created by liushiquan on 2018/11/12.
 */

public class GitLoginActivity extends AppCompatActivity implements View.OnClickListener{

    private Button login;

    private EditText account,password;

    private static final int SUCCESS = 1;

    private Handler mHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case SUCCESS:
                    openGitUser(msg.obj);
                    break;
                default:
                    break;
            }
        }

    };

    private void openGitUser(Object obj) {
        Intent intent = new Intent(this,GitUserInfoActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        ItheimaHttp.init(this, "https://api.github.com/");
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.git_login);
        account = findViewById(R.id.et_account);
        password  = findViewById(R.id.et_password);
        login = findViewById(R.id.btn_login);
        login.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:
                Request request = ItheimaHttp.newGetRequest("user");//apiUrl格式："xxx/xxxxx"
                request.putHeader("Authorization","Basic "+ Base64Utils.setEncryption(account.getText() + ":" + password.getText()));
                ItheimaHttp.send(request, new HttpResponseListener<GitUser>() {
                    @Override
                    public void onResponse(GitUser bean, Headers headers) {
                        bean.setAuthorization("Basic "+ Base64Utils.setEncryption(account.getText() + ":" + password.getText()));
                        GitCache.setGitUser(bean);
//                        Log.d("Weather", JSONObject.wrap(bean).toString());
                        Message message = new Message();
                        message.what = SUCCESS;
                        mHandler.sendMessage(message);
                    }

//                    @Override
//                    public void onFailure(Call<ResponseBody> call, Throwable e) {
//                        Toast.makeText(GitLoginActivity.this,"登录失败，账号或密码有误！",Toast.LENGTH_SHORT).show();
//                    }
                });
                break;
        }
    }
}
