package com.leospring.weather.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.itheima.retrofitutils.ItheimaHttp;
import com.itheima.retrofitutils.Request;
import com.itheima.retrofitutils.listener.HttpResponseListener;
import com.leospring.weather.bean.GitRepository;
import com.leospring.weather.cache.GitCache;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Headers;
import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * Created by Administrator on 2018/11/12.
 */

public class GitUserInfoActivity extends AppCompatActivity {

    private static final int OPEN_REPOS = 1;
    private ImageView imageView;
    private TextView textView;
    private ListView listView;
    private List<String> data;

    private Handler mHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case OPEN_REPOS:
                    openRepos(msg.obj);
                    break;
                default:
                    break;
            }
        }

    };

    private void openRepos(Object obj) {
        Intent intent = new Intent(this,GitContentActivity.class);
        intent.putExtra("reposName",obj.toString());
        startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        ItheimaHttp.init(this, "https://api.github.com/");
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.git_user_info);
        listView = findViewById(R.id.lv_repos);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String reposName = data.get(position);

                Message message = new Message();
                message.what = OPEN_REPOS;
                message.obj = reposName;
                mHandler.sendMessage(message);

            }
        });
        imageView = findViewById(R.id.img_git);
        textView = findViewById(R.id.name_git);
        new DownloadImageTask(imageView).execute(GitCache.getGitUser().getAvatar_url());
        textView.setText(GitCache.getGitUser().getLogin());
        initGitUser();
    }

    private void initGitUser() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Request request = ItheimaHttp.newGetRequest("users/LuckySe7ens/repos");
                request.putHeader("Authorization",GitCache.getGitUser().getAuthorization());
                ItheimaHttp.send(request, new HttpResponseListener<List<GitRepository>>() {
                    @Override
                    public void onResponse(List<GitRepository> beans, Headers headers) {
                        data = new ArrayList<>();
                        for (GitRepository bean : beans) {
                            data.add(bean.getName());
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                                GitUserInfoActivity.this, android.R.layout.simple_list_item_1, data);
                        listView.setAdapter(adapter);
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable e) {
                    }
                });
            }
        }).start();
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

}
