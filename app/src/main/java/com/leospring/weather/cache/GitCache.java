package com.leospring.weather.cache;

import com.leospring.weather.bean.GitUser;

/**
 * Created by liushiquan on 2018/11/12.
 */

public class GitCache {

    private static GitUser gitUser;

    public static GitUser getGitUser() {
        return gitUser;
    }

    public static void setGitUser(GitUser gitUser) {
        GitCache.gitUser = gitUser;
    }
}
