package com.leospring.weather.utils;

/**
 * Created by Administrator on 2018/11/13.
 */

public enum RequestMethod {
    GET("GET"),
    PUT("PUT"),
    DELETE("DELETE"),
    POST("POST");
    private final String value;

    RequestMethod(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
