//package com.leospring.weather.utils;
//
//import android.location.Address;
//import android.location.Criteria;
//import android.location.Geocoder;
//import android.location.Location;
//import android.location.LocationListener;
//import android.location.LocationManager;
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.util.Log;
//
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.InputStreamReader;
//import java.util.List;
//import java.util.Locale;
//
///**
// * Created by Administrator on 2018/10/31.
// */
//
//public class MapUtil {
//
//    //监听GPS位置改变后得到新的经纬度
//    private LocationListener locationListener = new LocationListener() {
//        public void onLocationChanged(Location location) {
//            Log.e("location", location.toString() + "....");
//            // TODO Auto-generated method stub
//            if (location != null) {
//                //获取国家，省份，城市的名称
//                Log.e("location", location.toString());
////                List<Address> m_list = getAddress(location);
//                new MyAsyncExtue().execute(location);
////                Log.e("str", m_list.toString());
////                String city = "";
//////                if (m_list != null && m_list.size() > 0) {
//////                    city = m_list.get(0).getLocality();//获取城市
//////                }
////                city = m_list;
////                show_GPS.setText("location:" + m_list.toString() + "\n" + "城市:" + city + "\n精度:" + location.getLongitude() + "\n纬度:" + location.getLatitude() + "\n定位方式:" + location.getProvider());
//            } else {
//                show_GPS.setText("获取不到数据");
//            }
//        }
//
//        @Override
//        public void onStatusChanged(String provider, int status, Bundle extras) {
//
//        }
//
//        @Override
//        public void onProviderEnabled(String provider) {
//
//        }
//
//        @Override
//        public void onProviderDisabled(String provider) {
//
//        }
//
//    };
//
//    private Location getLocation() {
//        //获取位置管理服务
//
//        //查找服务信息
//        Criteria criteria = new Criteria();
//        criteria.setAccuracy(Criteria.ACCURACY_FINE); //定位精度: 最高
//        criteria.setAltitudeRequired(false); //海拔信息：不需要
//        criteria.setBearingRequired(false); //方位信息: 不需要
//        criteria.setCostAllowed(true);  //是否允许付费
//        criteria.setPowerRequirement(Criteria.POWER_LOW); //耗电量: 低功耗
////        String provider = myLocationManager.getBestProvider(criteria, true); //获取GPS信息
////        myLocationManager.requestLocationUpdates(provider,2000,5,locationListener);
////        Log.e("provider", provider);
////        List<String> list = myLocationManager.getAllProviders();
////        Log.e("provider", list.toString());
////
//        Location gpsLocation = null;
//        Location netLocation = null;
//        myLocationManager.addGpsStatusListener(myListener);
//        if (netWorkIsOpen()) {
//            //2000代表每2000毫秒更新一次，5代表每5秒更新一次
//            myLocationManager.requestLocationUpdates("network", 2000, 5, locationListener);
//            netLocation = myLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
//        }
//
//        if (gpsIsOpen()) {
//            myLocationManager.requestLocationUpdates("gps", 2000, 5, locationListener);
//            gpsLocation = myLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
//        }
//
//        if (gpsLocation == null && netLocation == null) {
//            return null;
//        }
//        if (gpsLocation != null && netLocation != null) {
//            if (gpsLocation.getTime() < netLocation.getTime()) {
//                gpsLocation = null;
//                return netLocation;
//            } else {
//                netLocation = null;
//                return gpsLocation;
//            }
//        }
//        if (gpsLocation == null) {
//            return netLocation;
//        } else {
//            return gpsLocation;
//        }
//    }
//
//    private boolean gpsIsOpen() {
//        boolean isOpen = true;
//        if (!myLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {//没有开启GPS
//            isOpen = false;
//        }
//        return isOpen;
//    }
//
//    private boolean netWorkIsOpen() {
//        boolean netIsOpen = true;
//        if (!myLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {//没有开启网络定位
//            netIsOpen = false;
//        }
//        return netIsOpen;
//    }
//
//    private class MyAsyncExtue extends AsyncTask<Location, Void, String> {
//
//        @Override
//        protected String doInBackground(Location... params) {
//            HttpClient client = new DefaultHttpClient();
//            StringBuilder stringBuilder = new StringBuilder();
//            HttpGet httpGet = new HttpGet("http://api.map.baidu.com/geocoder?output=json&location=23.131427,113.379763&ak=esNPFDwwsXWtsQfw4NMNmur1");
//            try {
//                HttpResponse response = client.execute(httpGet);
//                HttpEntity entity = response.getEntity();
//                InputStream inputStream = entity.getContent();
//                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
//                String b;
//                while ((b = bufferedReader.readLine()) != null) {
//                    stringBuilder.append(b + "\n");
//                }
//                inputStream.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            return stringBuilder.toString();
//        }
//
//        @Override
//        protected void onPostExecute(String m_list) {
//            super.onPostExecute(m_list);
//            Log.e("str", m_list.toString());
//            String city = "";
////                if (m_list != null && m_list.size() > 0) {
////                    city = m_list.get(0).getLocality();//获取城市
////                }
//            city = m_list;
//            show_GPS.setText("城市:" + city);
//        }
//    }
//
//    // 获取地址信息
//    private List<Address> getAddress(Location location) {
//        List<Address> result = null;
//        try {
//            if (location != null) {
//                Geocoder gc = new Geocoder(this, Locale.getDefault());
//                result = gc.getFromLocation(location.getLatitude(),
//                        location.getLongitude(), 1);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return result;
//    }
//}
