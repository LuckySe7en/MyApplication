package com.leospring.weather.bean;

/**
 * Created by Administrator on 2018/11/12.
 */

public class GitContent {

    /**
     * name : 404.html
     * path : 404.html
     * sha : b1a7f2ee896ee39a2db2e6e9c520f6ce2668e3ab
     * size : 463
     * url : https://api.github.com/repos/LuckySe7ens/LuckySe7ens.github.io/contents/404.html?ref=master
     * html_url : https://github.com/LuckySe7ens/LuckySe7ens.github.io/blob/master/404.html
     * git_url : https://api.github.com/repos/LuckySe7ens/LuckySe7ens.github.io/git/blobs/b1a7f2ee896ee39a2db2e6e9c520f6ce2668e3ab
     * download_url : https://raw.githubusercontent.com/LuckySe7ens/LuckySe7ens.github.io/master/404.html
     * type : file
     * _links : {"self":"https://api.github.com/repos/LuckySe7ens/LuckySe7ens.github.io/contents/404.html?ref=master","git":"https://api.github.com/repos/LuckySe7ens/LuckySe7ens.github.io/git/blobs/b1a7f2ee896ee39a2db2e6e9c520f6ce2668e3ab","html":"https://github.com/LuckySe7ens/LuckySe7ens.github.io/blob/master/404.html"}
     */

    private String name;
    private String path;
    private String sha;
    private int size;
    private String url;
    private String html_url;
    private String git_url;
    private String download_url;
    private String type;
    private LinksBean _links;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getSha() {
        return sha;
    }

    public void setSha(String sha) {
        this.sha = sha;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getHtml_url() {
        return html_url;
    }

    public void setHtml_url(String html_url) {
        this.html_url = html_url;
    }

    public String getGit_url() {
        return git_url;
    }

    public void setGit_url(String git_url) {
        this.git_url = git_url;
    }

    public String getDownload_url() {
        return download_url;
    }

    public void setDownload_url(String download_url) {
        this.download_url = download_url;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public LinksBean get_links() {
        return _links;
    }

    public void set_links(LinksBean _links) {
        this._links = _links;
    }

    public static class LinksBean {
        /**
         * self : https://api.github.com/repos/LuckySe7ens/LuckySe7ens.github.io/contents/404.html?ref=master
         * git : https://api.github.com/repos/LuckySe7ens/LuckySe7ens.github.io/git/blobs/b1a7f2ee896ee39a2db2e6e9c520f6ce2668e3ab
         * html : https://github.com/LuckySe7ens/LuckySe7ens.github.io/blob/master/404.html
         */

        private String self;
        private String git;
        private String html;

        public String getSelf() {
            return self;
        }

        public void setSelf(String self) {
            this.self = self;
        }

        public String getGit() {
            return git;
        }

        public void setGit(String git) {
            this.git = git;
        }

        public String getHtml() {
            return html;
        }

        public void setHtml(String html) {
            this.html = html;
        }
    }
}
