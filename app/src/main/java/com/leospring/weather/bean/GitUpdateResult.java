package com.leospring.weather.bean;

import java.util.List;

/**
 * Created by Administrator on 2018/11/14.
 */

public class GitUpdateResult {

    /**
     * content : {"name":"test.md","path":"test.md","sha":"0d5a690c8fad5e605a6e8766295d9d459d65de42","size":20,"url":"https://api.github.com/repos/LuckySe7ens/LuckySe7ens.github.io/contents/test.md?ref=master","html_url":"https://github.com/LuckySe7ens/LuckySe7ens.github.io/blob/master/test.md","git_url":"https://api.github.com/repos/LuckySe7ens/LuckySe7ens.github.io/git/blobs/0d5a690c8fad5e605a6e8766295d9d459d65de42","download_url":"https://raw.githubusercontent.com/LuckySe7ens/LuckySe7ens.github.io/master/test.md","type":"file","_links":{"self":"https://api.github.com/repos/LuckySe7ens/LuckySe7ens.github.io/contents/test.md?ref=master","git":"https://api.github.com/repos/LuckySe7ens/LuckySe7ens.github.io/git/blobs/0d5a690c8fad5e605a6e8766295d9d459d65de42","html":"https://github.com/LuckySe7ens/LuckySe7ens.github.io/blob/master/test.md"}}
     * commit : {"sha":"ae9db34e3dfa3f8372ad8f290a04300dfae7e4ab","node_id":"MDY6Q29tbWl0MTUwNjc3ODI3OmFlOWRiMzRlM2RmYTNmODM3MmFkOGYyOTBhMDQzMDBkZmFlN2U0YWI=","url":"https://api.github.com/repos/LuckySe7ens/LuckySe7ens.github.io/git/commits/ae9db34e3dfa3f8372ad8f290a04300dfae7e4ab","html_url":"https://github.com/LuckySe7ens/LuckySe7ens.github.io/commit/ae9db34e3dfa3f8372ad8f290a04300dfae7e4ab","author":{"name":"LuckySe7ens","email":"564892076@qq.com","date":"2018-11-14T07:19:26Z"},"committer":{"name":"LuckySe7ens","email":"564892076@qq.com","date":"2018-11-14T07:19:26Z"},"tree":{"sha":"c1798032937916166635283c04df9ddc23932e09","url":"https://api.github.com/repos/LuckySe7ens/LuckySe7ens.github.io/git/trees/c1798032937916166635283c04df9ddc23932e09"},"message":"commit from INSOMNIA","parents":[{"sha":"ec05780fe5737a0c55891e86f6a3806a2ce51826","url":"https://api.github.com/repos/LuckySe7ens/LuckySe7ens.github.io/git/commits/ec05780fe5737a0c55891e86f6a3806a2ce51826","html_url":"https://github.com/LuckySe7ens/LuckySe7ens.github.io/commit/ec05780fe5737a0c55891e86f6a3806a2ce51826"}],"verification":{"verified":false,"reason":"unsigned","signature":null,"payload":null}}
     */

    private ContentBean content;
    private CommitBean commit;

    public ContentBean getContent() {
        return content;
    }

    public void setContent(ContentBean content) {
        this.content = content;
    }

    public CommitBean getCommit() {
        return commit;
    }

    public void setCommit(CommitBean commit) {
        this.commit = commit;
    }

    public static class ContentBean {
        /**
         * name : test.md
         * path : test.md
         * sha : 0d5a690c8fad5e605a6e8766295d9d459d65de42
         * size : 20
         * url : https://api.github.com/repos/LuckySe7ens/LuckySe7ens.github.io/contents/test.md?ref=master
         * html_url : https://github.com/LuckySe7ens/LuckySe7ens.github.io/blob/master/test.md
         * git_url : https://api.github.com/repos/LuckySe7ens/LuckySe7ens.github.io/git/blobs/0d5a690c8fad5e605a6e8766295d9d459d65de42
         * download_url : https://raw.githubusercontent.com/LuckySe7ens/LuckySe7ens.github.io/master/test.md
         * type : file
         * _links : {"self":"https://api.github.com/repos/LuckySe7ens/LuckySe7ens.github.io/contents/test.md?ref=master","git":"https://api.github.com/repos/LuckySe7ens/LuckySe7ens.github.io/git/blobs/0d5a690c8fad5e605a6e8766295d9d459d65de42","html":"https://github.com/LuckySe7ens/LuckySe7ens.github.io/blob/master/test.md"}
         */

        private String name;
        private String path;
        private String sha;
        private int size;
        private String url;
        private String html_url;
        private String git_url;
        private String download_url;
        private String type;
        private LinksBean _links;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public String getSha() {
            return sha;
        }

        public void setSha(String sha) {
            this.sha = sha;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getHtml_url() {
            return html_url;
        }

        public void setHtml_url(String html_url) {
            this.html_url = html_url;
        }

        public String getGit_url() {
            return git_url;
        }

        public void setGit_url(String git_url) {
            this.git_url = git_url;
        }

        public String getDownload_url() {
            return download_url;
        }

        public void setDownload_url(String download_url) {
            this.download_url = download_url;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public LinksBean get_links() {
            return _links;
        }

        public void set_links(LinksBean _links) {
            this._links = _links;
        }

        public static class LinksBean {
            /**
             * self : https://api.github.com/repos/LuckySe7ens/LuckySe7ens.github.io/contents/test.md?ref=master
             * git : https://api.github.com/repos/LuckySe7ens/LuckySe7ens.github.io/git/blobs/0d5a690c8fad5e605a6e8766295d9d459d65de42
             * html : https://github.com/LuckySe7ens/LuckySe7ens.github.io/blob/master/test.md
             */

            private String self;
            private String git;
            private String html;

            public String getSelf() {
                return self;
            }

            public void setSelf(String self) {
                this.self = self;
            }

            public String getGit() {
                return git;
            }

            public void setGit(String git) {
                this.git = git;
            }

            public String getHtml() {
                return html;
            }

            public void setHtml(String html) {
                this.html = html;
            }
        }
    }

    public static class CommitBean {
        /**
         * sha : ae9db34e3dfa3f8372ad8f290a04300dfae7e4ab
         * node_id : MDY6Q29tbWl0MTUwNjc3ODI3OmFlOWRiMzRlM2RmYTNmODM3MmFkOGYyOTBhMDQzMDBkZmFlN2U0YWI=
         * url : https://api.github.com/repos/LuckySe7ens/LuckySe7ens.github.io/git/commits/ae9db34e3dfa3f8372ad8f290a04300dfae7e4ab
         * html_url : https://github.com/LuckySe7ens/LuckySe7ens.github.io/commit/ae9db34e3dfa3f8372ad8f290a04300dfae7e4ab
         * author : {"name":"LuckySe7ens","email":"564892076@qq.com","date":"2018-11-14T07:19:26Z"}
         * committer : {"name":"LuckySe7ens","email":"564892076@qq.com","date":"2018-11-14T07:19:26Z"}
         * tree : {"sha":"c1798032937916166635283c04df9ddc23932e09","url":"https://api.github.com/repos/LuckySe7ens/LuckySe7ens.github.io/git/trees/c1798032937916166635283c04df9ddc23932e09"}
         * message : commit from INSOMNIA
         * parents : [{"sha":"ec05780fe5737a0c55891e86f6a3806a2ce51826","url":"https://api.github.com/repos/LuckySe7ens/LuckySe7ens.github.io/git/commits/ec05780fe5737a0c55891e86f6a3806a2ce51826","html_url":"https://github.com/LuckySe7ens/LuckySe7ens.github.io/commit/ec05780fe5737a0c55891e86f6a3806a2ce51826"}]
         * verification : {"verified":false,"reason":"unsigned","signature":null,"payload":null}
         */

        private String sha;
        private String node_id;
        private String url;
        private String html_url;
        private AuthorBean author;
        private CommitterBean committer;
        private TreeBean tree;
        private String message;
        private VerificationBean verification;
        private List<ParentsBean> parents;

        public String getSha() {
            return sha;
        }

        public void setSha(String sha) {
            this.sha = sha;
        }

        public String getNode_id() {
            return node_id;
        }

        public void setNode_id(String node_id) {
            this.node_id = node_id;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getHtml_url() {
            return html_url;
        }

        public void setHtml_url(String html_url) {
            this.html_url = html_url;
        }

        public AuthorBean getAuthor() {
            return author;
        }

        public void setAuthor(AuthorBean author) {
            this.author = author;
        }

        public CommitterBean getCommitter() {
            return committer;
        }

        public void setCommitter(CommitterBean committer) {
            this.committer = committer;
        }

        public TreeBean getTree() {
            return tree;
        }

        public void setTree(TreeBean tree) {
            this.tree = tree;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public VerificationBean getVerification() {
            return verification;
        }

        public void setVerification(VerificationBean verification) {
            this.verification = verification;
        }

        public List<ParentsBean> getParents() {
            return parents;
        }

        public void setParents(List<ParentsBean> parents) {
            this.parents = parents;
        }

        public static class AuthorBean {
            /**
             * name : LuckySe7ens
             * email : 564892076@qq.com
             * date : 2018-11-14T07:19:26Z
             */

            private String name;
            private String email;
            private String date;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getDate() {
                return date;
            }

            public void setDate(String date) {
                this.date = date;
            }
        }

        public static class CommitterBean {
            /**
             * name : LuckySe7ens
             * email : 564892076@qq.com
             * date : 2018-11-14T07:19:26Z
             */

            private String name;
            private String email;
            private String date;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getDate() {
                return date;
            }

            public void setDate(String date) {
                this.date = date;
            }
        }

        public static class TreeBean {
            /**
             * sha : c1798032937916166635283c04df9ddc23932e09
             * url : https://api.github.com/repos/LuckySe7ens/LuckySe7ens.github.io/git/trees/c1798032937916166635283c04df9ddc23932e09
             */

            private String sha;
            private String url;

            public String getSha() {
                return sha;
            }

            public void setSha(String sha) {
                this.sha = sha;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }
        }

        public static class VerificationBean {
            /**
             * verified : false
             * reason : unsigned
             * signature : null
             * payload : null
             */

            private boolean verified;
            private String reason;
            private Object signature;
            private Object payload;

            public boolean isVerified() {
                return verified;
            }

            public void setVerified(boolean verified) {
                this.verified = verified;
            }

            public String getReason() {
                return reason;
            }

            public void setReason(String reason) {
                this.reason = reason;
            }

            public Object getSignature() {
                return signature;
            }

            public void setSignature(Object signature) {
                this.signature = signature;
            }

            public Object getPayload() {
                return payload;
            }

            public void setPayload(Object payload) {
                this.payload = payload;
            }
        }

        public static class ParentsBean {
            /**
             * sha : ec05780fe5737a0c55891e86f6a3806a2ce51826
             * url : https://api.github.com/repos/LuckySe7ens/LuckySe7ens.github.io/git/commits/ec05780fe5737a0c55891e86f6a3806a2ce51826
             * html_url : https://github.com/LuckySe7ens/LuckySe7ens.github.io/commit/ec05780fe5737a0c55891e86f6a3806a2ce51826
             */

            private String sha;
            private String url;
            private String html_url;

            public String getSha() {
                return sha;
            }

            public void setSha(String sha) {
                this.sha = sha;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public String getHtml_url() {
                return html_url;
            }

            public void setHtml_url(String html_url) {
                this.html_url = html_url;
            }
        }
    }
}
